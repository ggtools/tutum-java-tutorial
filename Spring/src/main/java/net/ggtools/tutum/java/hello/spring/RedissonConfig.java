package net.ggtools.tutum.java.hello.spring;

import org.redisson.Config;
import org.redisson.Redisson;
import org.redisson.SingleServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;

@Configuration
public class RedissonConfig {
    private static final Logger log = LoggerFactory.getLogger(RedissonConfig.class);

    @Value("${REDIS_ENV_REDIS_PASS:#{null}}")
    private String password;

    @Bean
    public Redisson redisson() {
        Config config = new Config();
        SingleServerConfig singleServerConfig = config.useSingleServer();
        singleServerConfig.setAddress("redis:6379");

        if (password != null) {
            singleServerConfig.setPassword(password);
        }

        log.info("Creating Redisson config to {} {} password", singleServerConfig.getAddress(), password == null ? "without" : "with");

        return Redisson.create(config);
    }
}
